import {  Page, Heading, Button, Layout, Card, DisplayText, IndexTable } from "@shopify/polaris";
import Tabs from "../components/Tabs";
// import PostModal from "../components/PostModal";
// import Tables from "../components/Tables";

import Link from 'next/link'

const Index = () => (
  <Page>      
    <Layout>

    {/* <Layout.Section>
      <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
      <DisplayText size="large">Influencers</DisplayText>
      <PostModal />
      </div>
    </Layout.Section> */}

    <Layout.Section>
      <Tabs />     
    </Layout.Section>

    {/* <Layout.Section> */}
      {/* <Tables />      */}
    {/* </Layout.Section> */}

    </Layout>
  </Page>
);

export default Index;
