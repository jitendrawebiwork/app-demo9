import React, {useCallback, useState} from 'react';
import {Icon, Button, Checkbox, DropZone, Modal, Stack, TextField, FormLayout} from '@shopify/polaris';
import axios from 'axios';
import {
    EditMajor, ViewMajor, DeleteMajor
  } from '@shopify/polaris-icons';


export default function EditModalExample(props) {
  
  const [active, setActive] = useState(false);
  // const [checked, setChecked] = useState(false);

  const toggleActive = useCallback(() => setActive((active) => !active), []);

  // const handleCheckbox = useCallback((value) => setChecked(value), []);
  const [influencerPut, setInfluencerPut] = useState([]);

  function createInfluencer() {
    const putInfluencer = {
      storeName: storeName,
      bio: bio,
      name: name,

    };
    const tableRowId = props.id;
    axios.put(`http://localhost:3000/influencer/${tableRowId}`, putInfluencer).then((respone) => {
      alert("Influencer Put");
      setActive(!active);
      setInfluencerPut(respone.data);
      console.log("----putInfluencer-----",putInfluencer);
            
    });
  };



  const [name, setName] = useState(props.name);
  const handleNameChange = useCallback((value) => setName(value), []);

  const [bio, setBio] = useState(props.bio);
  const handleBioChange = useCallback((value) => setBio(value), []);

  const [storeName, setStoreName] = useState(props.storeName);
  const handleStoreNameChange = useCallback((value) => setStoreName(value), []);

  const activator = <Button onClick={toggleActive}><Icon source={EditMajor} color="base" /></Button>;

  return (
    <div style={{height: '35px'}}>
      <Modal
        large
        activator={activator}
        open={active}
        onClose={toggleActive}
        title="Add Influencer Details"
        primaryAction={{
          content: 'Save Changes',
          onAction: createInfluencer,
        }}
        secondaryActions={[
          {
            content: 'Cancel',
            onAction: toggleActive,
          },
        ]}
      >
      <Modal.Section>
        <FormLayout>
          <TextField label="Influencer Name" 
          type="text"
          value={name}
          onChange={handleNameChange} 
          autoComplete="off" />
          <TextField
        type="text"
        label="Influencer Bio"
        value={bio}
        onChange={handleBioChange}
        autoComplete="off"
        />
          <TextField 
          type="text"
          label="Store Name"
          value={storeName}
          onChange={handleStoreNameChange} 
          autoComplete="off" />
        </FormLayout>
      </Modal.Section>
        
        <Modal.Section>
          <Stack vertical>
            <DropZone
              accept=".png"
              errorOverlayText="File type must be .csv"
              type="file"
              onDrop={() => {}}
            >
              <DropZone.FileUpload />
            </DropZone>
            {/* <Checkbox
              checked={checked}
              label="Overwrite existing customers that have the same email or phone"
              onChange={handleCheckbox}
            /> */}
          </Stack>
        </Modal.Section>
      </Modal>
    </div>
  );
}
