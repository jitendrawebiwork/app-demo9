import React, { useCallback, useState, useEffect, useRef } from 'react';
import { Icon, Button, Modal, TextContainer } from '@shopify/polaris';
import {
  EditMajor, ViewMajor, DeleteMajor
} from '@shopify/polaris-icons';
import axios from 'axios';
 
export default function ModalExample({deleteInfluencer}) {
  
  const [active, setActive] = useState(false);

  const handleChange = useCallback(() => setActive(!active), [active]);

  const [influencerDelete, setInfluencerDelete] = useState([]);


  // useEffect( () => {
  //   console.log(influencerDelete);
  // },[influencerDelete]);


  // const deleteInfluencer = () => {
  //   if (props && props.id) {
  //     const tableRowId = props.id;
  //     axios.delete(`http://localhost:3000/influencer/${tableRowId}`).then((response) => {
  //       setInfluencerDelete(response.data);
  //       alert("Influencer Deleted");
  //       setActive(!active);
  //       // console.log("----delete",props)
  
  //     });
  //   }
   
  // }

  const activator = <Button onClick={handleChange}><Icon source={DeleteMajor} color="base" /></Button>;

  return (
    <div style={{ height: 'auto' }}>
      <Modal
        activator={activator}
        open={active}
        onClose={handleChange}
        title="Do you want to Delete?"
        primaryAction={{
          destructive: true,
          content: 'Delete',
          onAction: () => {
            deleteInfluencer();
            setActive(!active);
          }
          // onClick: setInfluencerDelete,
          
        }}
        secondaryActions={[
          {
            content: 'Cancel',
            onAction: handleChange,
          },
        ]}
      >

        {/* <Modal.Section>
          <TextContainer>
            <p>
              Use Instagram posts to share your products with millions of
              people. Let shoppers buy from your store without leaving
              Instagram.
            </p>
          </TextContainer>
        </Modal.Section> */}
      </Modal>
    </div>
  );
}
