import React from 'react';
import axios from "axios";
import { Button, Icon, useIndexResourceState, Card, IndexTable, TextStyle } from '@shopify/polaris';
import {
  EditMajor, ViewMajor, DeleteMajor
} from '@shopify/polaris-icons';
import DeleteModal from './DeleteModal';
import EditModel from './EditModal';
import { useState } from 'react';
import { useEffect } from 'react';
import Swal from 'sweetalert2'


export default function SimpleIndexTableExample() {

  const [influencerGet, setInfluencerGet] = useState([]);

  useEffect(() => {
    axios.get('http://localhost:3000/influencer').then((response) => {
      setInfluencerGet(response.data);


    });
  }, []);


  const customers = [
    {
      id: '3411',
      url: 'customers/341',
      name: 'Mae Jemison',
      bio: 'Decatur, USA',
      storeName: 'pilothouse',
      // amountSpent: '$2,400',
    },
    {
      id: '2561',
      url: 'customers/256',
      name: 'Ellen Ochoa',
      bio: 'Los Angeles, USA',
      storeName: 'Trial Store',
      // amountSpent: '$140',
    },
  ];
  const resourceName = {
    singular: 'customer',
    plural: 'customers',
  };

  const handleChange = (id) => {
    Swal.fire({
      title: 'Are you sure you want to delete?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        if (id) {
          const tableRowId = id;
          axios.delete(`http://localhost:3000/influencer/${tableRowId}`).then((response) => {
            alert("Influencer Deleted");
            axios.get('http://localhost:3000/influencer').then((response) => {
              setInfluencerGet(response.data);
            });
          });
        }

        Swal.fire(
          'Deleted!',
          'Influencer has been deleted.',
          'success'
        )
      }
    })
  }
  const resourceIDResolver = (influencerGet) => {
    return influencerGet.node.id;
  };

  const { selectedResources, allResourcesSelected, handleSelectionChange } =
    useIndexResourceState(influencerGet, { resourceIDResolver });

  // const [state, setState] = useState([]);

  // const changeState = () => {
  //   setState();
  // };

  const rowMarkup = influencerGet.map(
    ({ _id, name, bio, storeName }, index) => (
      <IndexTable.Row id={_id} key={_id} selected={selectedResources.includes(_id)} position={index}>
        {/* {console.log("----_id-----", _id)} */}
        <IndexTable.Cell>
          <TextStyle variation="strong">{name}</TextStyle>
        </IndexTable.Cell>
        <IndexTable.Cell>{bio}</IndexTable.Cell>
        <IndexTable.Cell>{storeName}</IndexTable.Cell>
        <IndexTable.Cell><EditModel /></IndexTable.Cell>
        <IndexTable.Cell><Icon source={ViewMajor} color="base" /></IndexTable.Cell>
        <IndexTable.Cell>
          {/* <DeleteModal id={_id}/> */}
          {/* {console.log("----id-----",ids)} */}
          <Button onClick={() => handleChange(_id)}><Icon source={DeleteMajor} color="base" /></Button>
        </IndexTable.Cell>
      </IndexTable.Row>
    ),
  );


  // console.log("----influencerGet-----", influencerGet);

  return (
    <Card>
      <IndexTable
        resourceName={resourceName}
        itemCount={influencerGet.length}
        selectedItemsCount={
          allResourcesSelected ? 'All' : selectedResources.length
        }
        onSelectionChange={handleSelectionChange}
        headings={[
          { title: 'Name' },
          { title: 'Bio' },
          { title: 'Store Name' },
          { title: 'Edit' },
          { title: 'View' },
          { title: 'Delete' },
        ]}
      >
        {rowMarkup}
      </IndexTable>
    </Card>
  );
}
