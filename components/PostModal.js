import React, {useCallback, useState} from 'react';
import {Button, Checkbox, DropZone, Modal, Stack, TextField, FormLayout} from '@shopify/polaris';
import { useEffect } from 'react';
import axios from 'axios';

export default function LargeModalExample({influencerGet}) {
  const [active, setActive] = useState(false);
  // const [checked, setChecked] = useState(false);

  const toggleActive = useCallback(() => setActive((active) => !active), []);

  // const handleCheckbox = useCallback((value) => setChecked(value), []);

  const [influencerPost, setInfluencerPost] = useState([]);

  
  function editInfluencer() {
    const postInfluencer = {
      storeName: storeName,
      bio: bio,
      name: name,

    };
    axios.post('http://localhost:3000/influencer', postInfluencer).then((respone) => {
      alert("Influencer Post");
      setActive(!active);
      setInfluencerPost(respone.data);
      console.log("----postInfluencer-----",postInfluencer);
      influencerGet();         
    });
  };



  const [name, setName] = useState('');
  const handleNameChange = useCallback((value) => setName(value), []);

  const [bio, setBio] = useState('');
  const handleBioChange = useCallback((value) => setBio(value), []);

  const [storeName, setStoreName] = useState('');
  const handleStoreNameChange = useCallback((value) => setStoreName(value), []);

  const activator = <Button primary onClick={toggleActive}>Add New</Button>;

  // function checkModal() {
  // const toggleActive = useCallback(() => setActive((active) => !active), []);

  // }

  return (
    <div style={{height: '35px'}}>
      <Modal
        large
        activator={activator}
        open={active}
        onClose={toggleActive}
        title="Edit Influencer Details"
        primaryAction={{
          content: 'Add',
          onAction: () => {
            editInfluencer();
            
          }
          
        }}
        secondaryActions={[
          {
            content: 'Cancel',
            onAction: toggleActive,
          },
        ]}
      >
      <Modal.Section>
        
        <FormLayout>
          <TextField
          type="text"
          label="Influencer Name" 
          value={name}
          onChange={handleNameChange}
          autoComplete="off" />
          <TextField
        type="text"
        label="Influencer Bio"
        value={bio}
        onChange={handleBioChange}
        autoComplete="off"
        />
          <TextField
        type="text"
          label="Store Name" 
          value={storeName}
          onChange={handleStoreNameChange} 
          autoComplete="off" />
        </FormLayout>
      </Modal.Section>
        
        <Modal.Section>
          <Stack vertical>
            <DropZone
              accept=".png"
              errorOverlayText="File type must be .csv"
              type="file"
              onDrop={() => {}}
            >
              <DropZone.FileUpload />
            </DropZone>
            {/* <Checkbox
              checked={checked}
              label="Overwrite existing customers that have the same email or phone"
              onChange={handleCheckbox}
            /> */}
          </Stack>
        </Modal.Section>
      </Modal>
    </div>
  );
}
