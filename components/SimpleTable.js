import React, { useCallback, useState, useEffect } from 'react';
import {Icon, Card, DataTable, Page} from '@shopify/polaris';
import axios from 'axios';
import { EditMajor, ViewMajor, DeleteMajor } from '@shopify/polaris-icons';
// import DeleteModal2 from './DeleteModal2';
import DeleteModal from './DeleteModal';
import PutModel from './PutModal';
import PostModal from './PostModal';

export default function DataTableExample() {
  
    const [influencer, setInfluencer] = useState([]);

    const influencerGet = () => {
        axios.get('http://localhost:3000/influencer').then( (response) => {
            setInfluencer(response.data);
        setActive(!active);

            console.log("-------influencer", influencer);
        });
    }

    useEffect( () => {
        influencerGet();
    }, []);

  const [selectid, setSelectedid] = useState("");
  const [influencerDelete, setInfluencerDelete] = useState([]);
  
  const [active, setActive] = useState(false);

  // const handleChange = useCallback(() => setActive(!active), [active]);

  const deleteInfluencer = () => {
    console.log("selectid", selectid);
    const tableRowId = selectid;
      axios.delete(`http://localhost:3000/influencer/${tableRowId}`).then((response) => {
        setInfluencerDelete(response.data);
        // alert("Influencer Deleted");
        // console.log("----delete",props)
        influencerGet();
        // setActive(!active);
      });
   
  }

    
    const rows = influencer.map( ({_id, name, bio, storeName}, index) => [
      index+1, name, `${bio.substring(0, 20)}`,storeName,

      <div style={{ display: "flex" }}>
        
      <div 
      // className="view" 
      onClick={() => console.log("---View Icon")}
      >
        <Icon source={ViewMajor} color="base" />
        
      </div>
      <div 
      // className="view" shopdata={_id,bio,name} onClick={() => {
      //     setModalUpdate(true),  setUpdatedata({id:_id,name:name,bio:bio});
      //   }}
        >
        {/* <Icon source={EditMajor} color="base" /> */}
        <PutModel 
        id={_id}
        name={name}
        bio={bio}
        storeName={storeName}
        // onClick= { () => {
        //   setUpdatedata({id:_id,name:name,bio:bio,storeName:storeName})
        // }}
        />

      </div>
      <div
        // className="view"
        onClick={() => {
          setSelectedid(_id);
          // console.log("---deleteOnClick")
        }}
      >
        {/* <Icon source={DeleteMajor} color="base" /> */}
        <DeleteModal 
        // id={_id}
        deleteInfluencer={deleteInfluencer}
        // influencerGet={influencerGet} 
        // onClick={ () => {
        //   setSelectedid(_id);
        // }}
        
        />
      </div>
    </div>,

        
    ]
    );

  return (
    <div>
      <div style={{display:"flex", justifyContent:"right"}}>
        <PostModal influencerGet={influencerGet} />
      </div>
      <Card>
        
        <DataTable
          columnContentTypes={[
            'text',
            'text',
            'text',
            'text',
            'text',
          ]}
          headings={[
            'Index',
            'Name',
            'Bio',
            'Store Name',
            'Actions',
          ]}
          rows={rows}
        />
       
      </Card>
    </div>
  );
}
