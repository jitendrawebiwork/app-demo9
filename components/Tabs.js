import React, {useCallback, useState} from 'react';
import {Card, Tabs} from '@shopify/polaris';
import SimpleTable from './SimpleTable';
// import TablesSwal from './TablesSwal';
// import SimpleTable2 from './SimpleTable2';

export default function TabsExample() {
  const [selected, setSelected] = useState(0);

  const handleTabChange = useCallback(
    (selectedTabIndex) => setSelected(selectedTabIndex),
    [],
  );

  const tabs = [
    {
      id: 'all-customers-1',
      content: 'All',
      accessibilityLabel: 'All customers',
      panelID: 'all-customers-content-1',
      component: <SimpleTable />,
    },
    {
      id: 'accepts-marketing-2',
      content: 'Accepts marketing2',
      panelID: 'accepts-marketing-content-2',
      // component: <TablesSwal />,
    },
    
    
  ];

  return (
    <Card>
      <Tabs tabs={tabs} selected={selected} onSelect={handleTabChange}>
        <Card.Section title={tabs[selected].component}>
          {/* <p>Tab {selected} selected</p> */}
          
        </Card.Section>
      </Tabs>
      
    </Card>
  );
}
